# basicCMS
===========

**Version 0.0.1**

### Instrucciones para crear un CMS basico con Symfony, Doctrine, Sonata...

Los pasos que se describen acontinuacion fueron hechos en una MAC que previamente ya tenia instalado MAMP para servir paginas PHP. A continuiacion se presentan los pasos para instalar el CMS desde principio.

### Para instalar Symfony en MAC OX
```xml
$ sudo curl -LsS http://symfony.com/installer -o /usr/local/bin/symfony
$ sudo chmod a+x /usr/local/bin/symfony
```
### Para instalar Symfony en Windowx
`c:\> php -r "file_put_contents('symfony', file_get_contents('http://symfony.com/installer'));"`

### Creacion del proyecto
Se crea el proyecto (la carpeta donde voy a trabajar)
`$ symfony new proyecto`

###Se configuran los accesos para LOGS Y CACHE
`$ chmod -R a+rw app/logs app/cache`

### para este momento ya debe de funcionar
`http://localhost:8888/proyecto

### modificamos el parameters y creamos la DB, recordar PUERTO MYSQL en caso de estar en MAMP
`$ vi app/config/parameters.yml`

### Creamos la base de datos 
`mysql> CREATE DATABASE proyecto_db DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_spanish_ci;`

### INSTALAMOS SI ES QUE NO LO TENEMOS "COMPOSER"
`$ curl -sS https://getcomposer.org/installer | php`

### Instalamos DOCTRINE MIGRATIONS con COMPOSER.PHAR [**Fuente**](http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html)
`$ php composer.phar require doctrine/doctrine-migrations-bundle "~1.0"`

**Nota:**SI MARCA ERROR DE TIMEZONE y tienes MAMP ir por /etc/php.../ y generar php.ini a partir de php.ini.default

AGREGAR AL app/AppKernel.php el siguiente elemento al ARRAY de bundles
`new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),`

**Si tenemos MAMP y queremos exportar el SOCKET**
`$ sudo ln -s /Applications/MAMP/tmp/mysql/mysql.sock /tmp/mysql.sock`

### Instalamos DOCTRUNE FIXTURES [**Fuente**](http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html)
`$ php composer.phar require doctrine/doctrine-fixtures-bundle`

Lo agregamos al app/AppKernel.php en el IF de DESARROLLO
`$bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();`

### Instalamos JMS BUNDLE [**Fuente**](https://packagist.org/packages/jms/serializer-bundle)
`$ php composer.phar require jms/serializer-bundle`

Lo agregamos al app/AppKernel.php en el ARRAY DE BUNDLES
new JMS\SerializerBundle\JMSSerializerBundle(),

### Instalamos SONATA ADMIN [**Fuente**](https://sonata-project.org/bundles/admin/2-3/doc/getting_started/installation.html)
`$ php composer.phar require sonata-project/admin-bundle "~2.3"`

### Instalamos SONATA ADMIN STORE BUNDLE [**Fuente**](https://sonata-project.org/bundles/admin/2-3/doc/getting_started/installation.html)
`$ php composer.phar require sonata-project/doctrine-orm-admin-bundle "~2.3"`

AGREGAMOS AL app/AppKernel.php
```xml
	//--> SONATA ADMIN
	new Sonata\CoreBundle\SonataCoreBundle(),
	new Sonata\BlockBundle\SonataBlockBundle(),
	new Knp\Bundle\MenuBundle\KnpMenuBundle(),
	new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
	new Sonata\AdminBundle\SonataAdminBundle(),
	// <--- SONATA ADMIN
```

### INSTALAMOS dependencias de BOWER
`$ bower install ./vendor/sonata-project/admin-bundle/bower.json`

Agregamos la importacion de la configuracion en el archivo app/config/config.yml
    `- { resource: sonata_block.yml }`

Generamos este archivo que estamos queriendo importar 
```xml
# app/config/sonata_block.yml
	sonata_block:
	    default_contexts: [cms]
	    blocks:
	        # enable the SonataAdminBundle block
	        sonata.admin.block.admin_list:
	            contexts: [admin]
```
Preparamos el ambiente
```xml
$ php app/console cache:clear
$ php app/console assets:install --symlink 
```

**Nota:**Para direccionar a desarrollo modificar `web/.htaccess` cambiando todos los app.php por app_dev.php

### Instalamos SONATA USERBUNDLE
`$ php composer.phar require sonata-project/user-bundle "~2.2"`

AGREGAMOS al app/AppKernel.php en el array
			
```xml
			new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
			....
			....
			//--> SONATA USERBUNDLE
            new FOS\UserBundle\FOSUserBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            //<-- SONATA USERBUNDLE
```

AGREGAMOS EN EL sonata_block.yml en la sección de BLOCK
		sonata.user.block.menu:    # used to display the menu in profile pages
        sonata.user.block.account: # used to display menu option (login option)
        sonata.block.service.text: # used to if you plan to use Sonata user routes



A Symfony project created on October 28, 2015, 11:01 am.
