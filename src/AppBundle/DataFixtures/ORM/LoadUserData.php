<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Sonata\UserBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $em)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('dbautista@denumeris.com');
        $user->setPlainPassword('admin');
        $user->addRole('ROLE_ADMIN');
        $user->addRole('ROLE_SUPER_ADMIN');
        $user->setEnabled(true);
        $em->persist($user);
        // $em->flush();

        $user1 = new User();
        $user1->setUsername('user');
        $user1->setEmail('bautista@denumeris.com');
        $user1->setPlainPassword('user');
        $user1->setEnabled(true);
        $em->persist($user1);
        $em->flush();


    }
}