<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="pais")
*/
class Pais
{
   /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @ORM\Column(type="string", length=50)
    */
   protected $name;

   /**
    * @ORM\Column(type="string", length=4)
    */
   protected $iso;


    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    // *
    //  * Sets the value of id.
    //  *
    //  * @param mixed $id the id
    //  *
    //  * @return self
     
    // public function setId($id)
    // {
    //     $this->id = $id;

    //     return $this;
    // }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of iso.
     *
     * @return mixed
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * Sets the value of iso.
     *
     * @param mixed $iso the iso
     *
     * @return self
     */
    public function setIso($iso)
    {
        $this->iso = $iso;

        return $this;
    }

    public function __toString(){
       return (string)$this->name;
   }
}