<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="destino")
*/
class Destino
{
   /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @ORM\Column(type="string", length=100)
    */
   protected $name;

   /**
    * @ORM\Column(type="text")
    */
   protected $description;

   /**
    * @ORM\Column(type="string", length=140)
    */
   protected $shortDescription;

   /**
    * @ORM\ManyToOne(targetEntity="Pais", cascade={"all"}, fetch="LAZY")
    * @ORM\JoinColumn(name="pais_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
    */
   private $pais;

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    // *
    //  * Sets the value of id.
    //  *
    //  * @param mixed $id the id
    //  *
    //  * @return self
     
    // public function setId($id)
    // {
    //     $this->id = $id;

    //     return $this;
    // }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of shortDescription.
     *
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Sets the value of shortDescription.
     *
     * @param mixed $shortDescription the short description
     *
     * @return self
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Gets the value of pais.
     *
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Sets the value of pais.
     *
     * @param mixed $pais the pais
     *
     * @return self
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }
}