<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PaisAdmin extends Admin
{
   protected function configureFormFields(FormMapper $formMapper)
   {
       $formMapper->add('name', 'text');
       $formMapper->add('iso', 'text');
   }

   protected function configureDatagridFilters(DatagridMapper $datagridMapper)
   {
       $datagridMapper->add('name');
       $datagridMapper->add('iso');
   }

   protected function configureListFields(ListMapper $listMapper)
   {
       $listMapper->addIdentifier('id');
       $listMapper->addIdentifier('name');
       $listMapper->add('iso');
    }
}