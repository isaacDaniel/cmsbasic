<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DestinoAdmin extends Admin
{
   protected function configureFormFields(FormMapper $formMapper)
   {
       $formMapper->add('name', 'text');
       $formMapper->add('description', 'textarea');
       $formMapper->add('shortDescription', 'text');
       $formMapper->add('pais', 'sonata_type_model');
   }

   protected function configureDatagridFilters(DatagridMapper $datagridMapper)
   {
       $datagridMapper->add('name');
       // $datagridMapper->add('shortDescription');
       $datagridMapper->add('pais');
   }

   protected function configureListFields(ListMapper $listMapper)
   {
       $listMapper->addIdentifier('id');
       $listMapper->addIdentifier('name');
       $listMapper->add('shortDescription');
       $listMapper->add('pais');
   }
}