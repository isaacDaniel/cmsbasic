<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151104132950 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE destino ADD pais_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE destino ADD CONSTRAINT FK_81F64EFAC604D5C6 FOREIGN KEY (pais_id) REFERENCES pais (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_81F64EFAC604D5C6 ON destino (pais_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE destino DROP FOREIGN KEY FK_81F64EFAC604D5C6');
        $this->addSql('DROP INDEX IDX_81F64EFAC604D5C6 ON destino');
        $this->addSql('ALTER TABLE destino DROP pais_id');
    }
}
